/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Fournisseur;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

/**
 *
 * @author Tsiry
 */
public class FournisseurDAO {
    public List<Fournisseur> getAllFournisseur(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null ;
        try{
            t=session.beginTransaction();
            List<Fournisseur> list = session.createQuery("from Fournisseur").list();
            t.commit();
            return list;
            
        }catch(Exception e){
            if(t!=null){
                t.rollback();
            }
        }finally{session.close();}
        return null ;
    }
    
    
    public boolean saveFournisseur(Fournisseur f) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null;

        try {
            t = session.beginTransaction();
            session.save(f);
            t.commit();
            return true;
        } catch (Exception e) {
            if (!(t == null)) {
                t.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }
    
    public Fournisseur getFournisseur(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null;
        Fournisseur fournisseur = null;
        try {
            t = session.beginTransaction();
            fournisseur = (Fournisseur) session.get(Fournisseur.class, id);
            t.commit();
            return fournisseur;
        } catch (Exception e) {
            if (!(t == null)) {
                t.rollback();
            }
        } finally {
            session.close();
        }
        return null;
    }
    
     public boolean updateFournisseur(Fournisseur f) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null;

        try {
            t = session.beginTransaction();
            session.update(f);
            t.commit();
            return true;
        } catch (Exception e) {
            if (!(t == null)) {
                t.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }
     
    public boolean deleteFournisseur(int id) {
        String hql = "delete from Fournisseur where fid = :id";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null;

        try {
            t = session.beginTransaction();
            Query query = session.createSQLQuery(hql);
            query.setInteger("id", id);
            query.executeUpdate();
            t.commit();
            return true;
        } catch (Exception e) {
            if (!(t == null)) {
                t.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }
}
