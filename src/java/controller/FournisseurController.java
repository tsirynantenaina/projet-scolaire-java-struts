/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import dao.FournisseurDAO;
import entity.Fournisseur;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author Tsiry
 */
public class FournisseurController extends ActionSupport{
    private Fournisseur fournisseur = new Fournisseur();
    private FournisseurDAO dao = new FournisseurDAO();
    private List<Fournisseur> fournisseurList ;
    private String erreurMessage ="";
    private String succesMessage="";

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public FournisseurDAO getDao() {
        return dao;
    }

    public void setDao(FournisseurDAO dao) {
        this.dao = dao;
    }

    public List<Fournisseur> getFournisseurList() {
        return fournisseurList;
    }

    public void setFournisseurList(List<Fournisseur> fournisseurList) {
        this.fournisseurList = fournisseurList;
    }

    public String getErreurMessage() {
        return erreurMessage;
    }

    public void setErreurMessage(String erreurMessage) {
        this.erreurMessage = erreurMessage;
    }

    public String getSuccesMessage() {
        return succesMessage;
    }

    public void setSuccesMessage(String succesMessage) {
        this.succesMessage = succesMessage;
    }

    @Override
    public String execute() throws Exception {
        this.fournisseurList = dao.getAllFournisseur();
        return SUCCESS;
    }
    
    public String insertFournisseur() {
        fournisseur.setFnom(fournisseur.getFnom());
        fournisseur.setFtel(fournisseur.getFtel());
        boolean status = dao.saveFournisseur(fournisseur);
        if (status) {
            setSuccesMessage("Fournisseur bien enregistré");
            this.fournisseurList = dao.getAllFournisseur();
            return SUCCESS;
        } else {
            setErreurMessage("Erreur d'enregistrement fournisseur");
            return "input";
        }

    }
    
    public String editFournisseur() {
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        int fid = Integer.parseInt(request.getParameter("fid"));

        if (dao.getFournisseur(fid) != null) {
            Fournisseur f = dao.getFournisseur(fid);
            fournisseur.setFid(f.getFid());
            fournisseur.setFnom(f.getFnom());
            fournisseur.setFtel(f.getFtel());
            return SUCCESS;
        }

        return "input";

    }
    
    public String updateFournisseur(){
       fournisseur.setFid(fournisseur.getFid());
       fournisseur.setFnom(fournisseur.getFnom());
       fournisseur.setFtel(fournisseur.getFtel());
       boolean status = dao.updateFournisseur(fournisseur);
        if (status) {
            setSuccesMessage("Fournisseur bien modifier");
            this.fournisseurList = dao.getAllFournisseur();
            return SUCCESS;
        } else {
            setErreurMessage("Erreur de modification fournisseur");
            return "input";
        }
       
    }
    
    public String deleteFournisseur() {
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        int fid = Integer.parseInt(request.getParameter("fid"));

        boolean status = dao.deleteFournisseur(fid);

        if (status) {
            setSuccesMessage("Fournisseur supprimé");
            this.fournisseurList = dao.getAllFournisseur();
            return SUCCESS;
        } else {
            setErreurMessage("Erreur de suppression");
            return "input";
        }
    }
    
}
