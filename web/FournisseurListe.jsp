<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Stock CSB</title>

        <!-- Bootstrap Core CSS -->
        <link href="template/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="template/css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="template/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="">Gestion stock CSB</a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                

                <ul class="nav navbar-right navbar-top-links">
                    
                        
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> se deconnecter <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Parametre</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Quitter</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Acceuil</a>
                            </li>
                            
                            <li>
                                <a href="#"><i class="fa fa-table fa-fw"></i> Fournisseur</a>
                            </li>
                            <li>
                                <a href="ProduitListe.html"><i class="fa fa-edit fa-fw"></i> Produit</a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Fournisseur</h1>
                        </div>
                        
                        <!-- /.col-lg-12 -->
                        
                    </div>
                    <!-- /.row -->
                    <s:if test="succesMessage != null">
                       
                        <div class="alert alert-success alert-dismissible made modal" id="myModaltest"  role="alert" aria-hidden="true" aria-hidden="true" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <p><s:property value="succesMessage"/></p>
                        </div>   
                    </s:if>
                    <s:if test="erreurMessage != null">
                        
                    </s:if>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                        Nouveau fournisseur
                                    </button>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nom Frns</th>
                                                    <th>Telephone</th>
                                                    <th colspan="2">Actions</th>
                                                </tr>
                                            </thead>
                           
                                            <tbody>
                                            <c:forEach var="row" items="${fournisseurList}">
                                                <tr>
                                                    <td>${row.fid}</td>
                                                    <td>${row.fnom}</td>
                                                    <td>${row.ftel}</td>
                                                    <td><button class="btn btn-warning editFournisseur" data-toggle="modal" data-target="#myModalEdit">Modifier</button> </td>
                                                    <td><button  class="btn btn-danger deleteFournisseur" data-toggle="modal" data-target="#myModalDelete">Supprimer</button></td>
                                                    
                                                    <!--<td><a href="edit.html?fid=${row.fid}">Edit</a></td>
                                                    <td><a href="delete.html?fid=${row.fid}">Delete</a></td>-->
                                                </tr>
                                            </c:forEach>               
                        
                                            </tbody>
                                              
                                        </table>
                                    </div>
                                    
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    
                    
                </div>
                        
            </div>
            <!-- /#page-wrapper -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Formulaire Fournisseur</h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                    <form action="insertFournisseur.html" method="post">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="fournisseur.fnom" placeholder="Nom fournisseur" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="fournisseur.ftel" placeholder="Telephone" required>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <input type="submit" value="Enregistrer" class="btn btn-primary" >
                                                    </form> 
                                                </div>
                                                
                                                
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>

        </div>
        <!-- /#wrapper -->
        
        
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Editer Fournisseur</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post"  action="updateFournisseur.html">
                                                        <input type="hidden"  name="fournisseur.fid" id="idEdit"    placeholder="Entrer nom">
                                                        <div class="form-group">
                                                            <input class="form-control" name="fournisseur.fnom"  id="nomEdit" placeholder="Nom fournisseur">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="form-control" name="fournisseur.ftel" id="telEdit" placeholder="Telephone">
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" value="Modifier" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Modifier</button>
                                                    </form> 
                                                </div>
                                                
                                                
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>

        </div>
        <!-- /#wrapper -->
        
        <div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog" role="document" style="width:20% ; min-width:200px " >
                                            <div class="modal-content">
                                                <div class="modal-header" >
                                                    
                                                    <h4 class="modal-title" id="myModalLabel" >Confirmation de suppression </h4>
                                                </div>
                                                <div class="modal-body" >
                                                    <form method="get" action="delete.html">
                                                        <input type="hidden"  name="fid" id="idSuppr" >
                                                        <div class="form-group" id="msgConf">
                                                            
                                                        </div>
                                                        
                                                </div>
                                                <div class="modal-footer" >
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">NON</button>
                                                    <button type="submit"  class="btn btn-primary" >OUI</button>
                                                    </form> 
                                                </div>
                                                
                                                
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
            
            
        
        </div>  

        <!-- jQuery -->
        <script src="./template/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="./template/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="./template/js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="./template/js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Notifications - Use for reference -->
        <script>
            // tooltip demo
            $('.tooltip-demo').tooltip({
                selector: "[data-toggle=tooltip]",
                container: "body"
            })

            // popover demo
            $("[data-toggle=popover]").popover()
            
            //getDataTable edit
            $(document).ready(function(){
            $(".editFournisseur").on('click', function(){
             $("#modal-success").modal("show");
             $tr=$(this).closest("tr");
             var data=$tr.children("td").map(function(){
                return $(this).text();
             }).get();
              console.log(data);
              $("#idEdit").val(data[0]);
              $("#nomEdit").val(data[1]);
              $("#telEdit").val(data[2]);

            });

    });
            
            //getDataTable delete
            $(document).ready(function(){
            $(".deleteFournisseur").on('click', function(){
             $("#modal-success").modal("show");
             $tr=$(this).closest("tr");
             var data=$tr.children("td").map(function(){
                return $(this).text();
             }).get();
              console.log(data);
              var phrase ="Voulez vous vraiment supprimer le fournisseur numero "+data[0]+" ?";
              $("#idSuppr").val(data[0]);
              document.getElementById('msgConf').innerHTML="<p style=\"text-align:center\">"+phrase+"</p>";
       
              
            });

    });
    
    
    
    
        </script>

    </body>
</html>
